# Raul

Aplicação para uso na disciplina Sistemas Distribuídos.

## Processo

1 - A aplicação Spring foi criada;

2 - Em application.properties foi adicionado:
- `server.port=${WSPORT:8081}`

3 - Use mvn para o .jar da aplicação ser criado:

- `mvn clean package`

4 - O Dockerfile foi criado:

```
FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
CMD ["java","-jar","/app.jar", "WSPORT"]
# A linha acima executará o "app.jar" enviando o parâmetro WSPORT que deve ser uma variável de ambiente do container. 
```

5 - A aplicação foi enviada para um repositório git;

6 - Dentro do servidor foi clonada a aplicação;

7 - A imagem da aplicação foi construída e o container criado (a porta escolhida foi a 8080):

- `docker build --tag raul/firstdeploy . `
- `docker run -e WSPORT=8080 -p 8087:8080 --name app_raul raul/firstdeploy`

