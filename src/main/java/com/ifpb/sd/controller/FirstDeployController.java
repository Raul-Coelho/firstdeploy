package com.ifpb.sd.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author raul
 * @project FirstDeploy
 */
@RestController
@RequestMapping("/status")
public class FirstDeployController {
    @GetMapping()
    private String getOk(){
        return "Ok";
    }
}
